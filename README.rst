pyOpenMI
----------------

Python implementation of **OpenMI 2.0** standard (http://www.openmi.org/), one of **OGC** standards (http://www.opengeospatial.org/standards/openmi). This is in test phase and not part of official standard.



Contents
========

openmi/ - OpenMI 2.0 API implementation (not complete yet)

openmi/test/ogimet/ - test OpenMI 2.0 component (temperature provider from Ogimet for Krakow station)

openmi/test/testogimet/ - sample use of Ogimet component


Usage
=====

To run sample, download the code and execute::
    
    $ cd openmi/test/testogimet/
    $ python main.py
    > Python OpenMI test implementation
    > ----------------------------------------
    > Station: Krakow
    > Local station time:  2014-05-12 19:03:03.103663+02:00
    > Initialize ...
    > Recent air temperature: 12.8 [C]
    > Finish ...