#!/usr/bin/python
# -*- coding: utf-8 -*-
"""ITimeSpaceValueSet class"""


"""
Copyright (c) 2005-2010, OpenMI Association
"http://www.openmi.org/"
Python version by Robert Szczepanek, 2011-2014
"http://www.openhydrology.org/"

This file is __NOT__YET__ part of OpenMI.Standard2

OpenMI.Standard2 is free software; you can redistribute it and/or modify
it under the terms of the Lesser GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

OpenMI.Standard2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
Lesser GNU General Public License for more details.

You should have received a copy of the Lesser GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from ibasevalueset import IBaseValueSet

class ITimeSpaceValueSet(IBaseValueSet):
    """
    """

    def GetElementValuesForTime():
        """

        GetElementValuesForTime(int) : IList
        """

    def GetTimeSeriesValuesForElement():
        """

        GetTimeSeriesValuesForElement(int) : IList
        """

    def GetValue():
        """
        GetValue(int, int) : object
        """

    def SetElementValuesForTime():
        """

        SetElementValuesForTime(IList, int) : void
        """

    def SetTimeSeriesValuesForElement():
        """

        SetTimeSeriesValuesForElement(IList, int) : void
        """

    def SetValue():
        """

        SetValue(object, int, int) : void
        """


    def Values2D():
        """

        Values2D() : IList<IList>
        """
