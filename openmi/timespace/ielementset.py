"""
Copyright (c) 2005-2010, OpenMI Association
"http://www.openmi.org/"
Python version by Robert Szczepanek, 2011
"http://www.openhydrology.org/"

This file is __NOT__YET__ part of OpenMI.Standard2

OpenMI.Standard2 is free software; you can redistribute it and/or modify
it under the terms of the Lesser GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

OpenMI.Standard2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
Lesser GNU General Public License for more details.

You should have received a copy of the Lesser GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

class IElementSet(ISpatialDefinition):
    """
    Data exchange between components in OpenMI is nearly always related to one or
    more elements in a space, either geo-referenced or not. An element set in
    OpenMI can be a list of 2D or 3D spatial elements or, as a special case, a
    list of ID based (non spatial) elements. The latter is supported to allow the
    exchange of arbitrary data that is not related to space in any way. Possible
    element types are defined in ElementType.
    
    An IElementSet is composed of an ordered list of elements having a common
    type. The geometry of each element is described by an ordered list of
    vertices. For 3D elements (i.e. polyhedrons) the shape can be queried by
    face. When the element set is geo-referenced co-ordinates (X,Y,Z,M) can be
    obtained for each vertex of an element.

    A geo-referenced element set needs to have a valid SpatialReferenceSystemWkt
    property set in the ISpatialDefinition. This is a string that
    specifies the OGC Well-Known Text representation of the spatial reference. An
    empty string indicates that there is no spatial reference, which is only
    valid if the ElementType is Id_Based.

    While an IElementSet can be used to query the geometric description of a
    model schematization, it does not necessarily provide all topological
    knowledge on inter-element connections.

    Although most models encapsulate static element sets, some advanced models
    might contain dynamic elements (e.g. waves). A version number has been
    introduced to enable tracking of element set changes over time. If the
    version changes, the element set might need to be queried again during the
    computation process.

    For ElementSets of type ElementType.IdBased the SpatialReferenceSystemWkt property an empty string.
    """

    def GetElementId(self, index):
        """
        GetElementId(int) : IIdentifiable
        
        Returns Id of the index-th element in the ElementSet. Indexes start from zero.
        If the ElementType of the ElementSet is not IdBased, a null or an empty string may be returned.
        The element index for which the element Caption is requested. If the element index is outside
        the range [0, number of elements -1], an exception must be thrown.
        The id of the element with the specified index.
        If the index is out of range, an exception must be thrown.
        """
        pass

    def GetElementIndex(self, elementId):
        """
        GetElementIndex(IIdentifiable) : int

        Index of element with id elementId in the elementset. Indexes start from zero.
        There are not restrictions to how elements are ordered.

        elementId
        Identification string for the element for which the element index is requested.
        Index of the element with the specified id.
        If no element in the ElementSet has the specified elementId, -1 must be returned.
        """
        pass

    def GetFaceCount(self, elementIndex):
        """
        GetFaceCount(int) : int
        
        Returns the number of faces in a 3D element. For 2D elements this returns 0.
        elementIndex
        Index for the element

        If the element index is outside the range [0, number of elements -1], an exception
        must be thrown.
        """
        pass

    def GetFaceVertexIndices(self, elementIndex, faceIndex):
        """
        GetFaceVertexIndices(int, int) : int[]
        
        Gives an array with the vertex indices for a face. 
        elementIndex - Element index
        faceIndex - Face index
        The vertex indices for this face.
    
        The vertex indices for a face must be locally numbered for the element
        (containing numbers in the range [0;GetVertexCount(elementIndex)-1]).
        """
        pass

    def GetVertexCount(self, elementIndex):
        """
        GetVertexCount(int) : int
        
        Number of vertices for the element specified by the elementIndex.

        If the GetVertexCount()method is invoked for element sets of type 
        TimeSpace.ElementType.IdBased, an exception must be thrown.
        
        elementIndex - The element index for the element for which the 
        number of vertices is requested. If the element index is outside 
        the range [0, number of elements -1], an exception must be thrown.
    
        Returns number of vertices in element defined by the elementIndex.
        """
        pass

    def GetVertexMCoordinate(self, elementIndex, vertexIndex):
        """
        GetVertexMCoordinate(int, int) : double
        
        M co-ordinate for the vertex with VertexIndex of the element with elementIndex.
    
        elementIndex - Element index.
        vertexIndex - Vertex index in the element with index elementIndex.
        """
        pass

    def GetVertexXCoordinate(self, elementIndex, vertexIndex):
        """
        GetVertexXCoordinate(int, int) : double

        X co-ordinate for the vertex with VertexIndex of the element with elementIndex.
    
        elementIndex - Element index.
        vertexIndex - Vertex index in the element with index elementIndex.
        """
        pass

    def GetVertexYCoordinate(self, elementIndex, vertexIndex):
        """
        GetVertexYCoordinate(int, int) : double

        Y co-ordinate for the vertex with VertexIndex of the element with elementIndex.
    
        elementIndex - Element index.
        vertexIndex - Vertex index in the element with index elementIndex.
        """
        pass

    def GetVertexZCoordinate(self, elementIndex, vertexIndex):
        """
        GetVertexZCoordinate(int, int) : double
        Z co-ordinate for the vertex with VertexIndex of the element with elementIndex.
    
        elementIndex - Element index.
        vertexIndex - Vertex index in the element with index elementIndex.
        """
        pass

    def ElementType(self):
        """
        ElementType() : ElementType

        ElementType of the elementset. All elements in the set are of his type.
        """
        pass

    def HasM(self):
        """
        HasM() : bool
        
        True if the element set supports M co-ordinates.
        """
        pass

    def HasZ(self):
        """
        HasZ() : bool

        True if the element set supports Z co-ordinates.
        """
        pass
