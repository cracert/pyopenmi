#!/usr/bin/python
# -*- coding: utf-8 -*-
"""IDescribable class"""


"""
Copyright (c) 2005-2010, OpenMI Association
"http://www.openmi.org/"
Python version by Robert Szczepanek, 2011-2014
"http://www.openhydrology.org/"

This file is __NOT__YET__ part of OpenMI.Standard2

OpenMI.Standard2 is free software; you can redistribute it and/or modify
it under the terms of the Lesser GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

OpenMI.Standard2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
Lesser GNU General Public License for more details.

You should have received a copy of the Lesser GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


class IDescribable(object):
    """ Provides descriptive information on an OpenMI entity."""


    def __init__(self, caption=None, description=None):
        self.caption = caption
        self.description = description


    def Caption(self):
        """Get the caption of IDescribable object"""
        return self.caption


    def Description(self):
        """Get additional description of IDescribable object"""
        return self.description


    def SetCaption(self, name):
        """Set caption for IDescribable object

        Not to be used as an id.
        Caption is used for eg. in GUI as element for selection."""
        self.caption = name


    def SetDescription(self, name):
        """Set description for IDescribable object"""
        self.description = name
