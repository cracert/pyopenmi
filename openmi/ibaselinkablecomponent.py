#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Copyright (c) 2005-2010, OpenMI Association
"http://www.openmi.org/"
Python version by Robert Szczepanek, 2011-2014
"http://www.openhydrology.org/"

This file is __NOT__YET__ part of OpenMI.Standard2

OpenMI.Standard2 is free software; you can redistribute it and/or modify
it under the terms of the Lesser GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

OpenMI.Standard2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
Lesser GNU General Public License for more details.

You should have received a copy of the Lesser GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


import abc
from idescribable import IDescribable

class IBaseLinkableComponent(IDescribable):
    """
    """
    __metaclass__  = abc.ABCMeta


    @abc.abstractmethod
    def Finish(self):
        """

        Finish() : void
        """


    @abc.abstractmethod
    def Initialize(self):
        """

        Initialize(IArgument[]) : void
        """


    #@abc.abstractmethod
    def Prepare(self):
        """

        Prepare() : void
        """


    #@abc.abstractmethod
    def Update(self):
        """

        Update(IOutput[]) : void
        """


    #@abc.abstractmethod
    def Validate(self):
        """

        Validate() : string[]
        """

    def AdaptedOutputFactories(self):
        """

        AdaptedOutputFactories() : List<IAdaptedOutputFactory>
        """

    def Arguments(self):
        """

        Arguments() : IArgument[]
        """

    def CascadingUpdateCallsDisabled(self):
        """

        CascadingUpdateCallsDisabled() : bool
        """


    #@abc.abstractmethod
    def Inputs(self):
        """

        Inputs() : IList<IBaseInput>
        """


    #@abc.abstractmethod
    def Outputs(self):
        """

        Outputs() : IList<IBaseOutput>
        """


    #@abc.abstractmethod
    def Status(self):
        """

        Status() : LinkableComponentStatus
        """


    def TimeExtent(self):
        """

        TimeExtent() : ITimeSet
        """

    def ExchangeItemValueChanged(self):
        """

        ExchangeItemValueChanged() : EventHandler<ExchangeItemChangeEventArgs>
        """


    #@abc.abstractmethod
    def StatusChanged(self):
        """

        StatusChanged() : EventHandler<LinkableComponentStatusChangeEventArgs>
        """
