"""
Copyright (c) 2005-2010, OpenMI Association
"http://www.openmi.org/"
Python version by Robert Szczepanek, 2011
"http://www.openhydrology.org/"

This file is __NOT__YET__ part of OpenMI.Standard2

OpenMI.Standard2 is free software; you can redistribute it and/or modify
it under the terms of the Lesser GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

OpenMI.Standard2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
Lesser GNU General Public License for more details.

You should have received a copy of the Lesser GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

class IQuality(IValueDefinition):
    """
    Qualitative data described items in terms of some quality or categorization
    that may be 'informal' or may use relatively ill-defined characteristics such
    as warmth and flavour. However, qualitative data can include well-defined
    aspects such as gender, nationality or commodity type. OpenMI defines the
    IQuality interface for working with qualitative data.

    An IQuality describes qualitative data, where a value is specified as one
    category within a number of predefined (possible) categories. These
    categories can be ordered or not.

    For qualitative data the IValueSet exchanged between ILinkableComponents
    contains one of the possible ICategory instances per element in the
    ElementSet involved.

    Examples: 
    Colors: red, green, blue 
    Land use: nature, recreation, industry, infrastructure 
    Rating: worse, same, better
    """

    def IsOrdered(self):
        """
        Checks if the IQuality is defined by an ordered set of ICategory or not.

        Return True if the categories are ordered
        """
        pass


    def Categories(self):
        """
        Returns a collection of the possible ICategory allowed for this
        IQuality. If the quality is not ordered the sequence of ICategory in
        the returned collection may vary. When the quality is ordered the items
        in the collection should always be in the same sequence.

        Return Collection of ICategory defined by the IQuality       
        """
        pass

        
