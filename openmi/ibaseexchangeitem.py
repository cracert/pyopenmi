#!/usr/bin/python
# -*- coding: utf-8 -*-
"""IBaseExchangeItem class"""


"""
Copyright (c) 2005-2010, OpenMI Association
"http://www.openmi.org/"
Python version by Robert Szczepanek, 2011-2014
"http://www.openhydrology.org/"

This file is __NOT__YET__ part of OpenMI.Standard2

OpenMI.Standard2 is free software; you can redistribute it and/or modify
it under the terms of the Lesser GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

OpenMI.Standard2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
Lesser GNU General Public License for more details.

You should have received a copy of the Lesser GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from iidentifiable import IIdentifiable
from ibaselinkablecomponent import IBaseLinkableComponent


class IBaseExchangeItem(IIdentifiable):
    """
    An item that can be exchanged, either as input or as output.
    """

    def Component(self):
        """
        Gets the owner of the exchange item. For an output exchange item this is
        the component responsible for providing the content of the output item.
        It is possible for an exchange item to have no owner, in this case the
        method will return null.

        @return ILinkableComponent owning the exchange item, can be null
        """
        return self.IBaseLinkableComponent


    def ValueDefinition(self):
        """
        Definition of the values in the exchange item.
        The {@link IValueDefinition} should never be returned directly; all
        implementing classes should return an {@link IQuality}, an
            {@link IQuantity}, or a custom derived value definition interface.

            @return Definition of the values in the exchange item
        """

        return self.IValueDefinition



#    def ElementSet(self):
#        """
#        Element set information (e.g. spatial) on the values that are available
#        in an output exchange item, or required by an input exchange item.
#
#        @return IElementSet containing the information
#        """
#
#        return self.IElementSet
#
#    def TimeSet(self):
#        """
#        Element set information (e.g. spatial) on the values that are available
#        in an output exchange item, or required by an input exchange item.
#
#        @return IElementSet containing the information
#        """
#
#        return self.ITimeSet