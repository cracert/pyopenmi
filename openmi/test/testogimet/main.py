#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Test for Ogimet OpenMI component"""

__author__      = "Robert Szczepanek"
__email__       = "robert@szczepanek.pl"

import sys
import os
import datetime
import pytz

a_path = os.path.abspath('..')
sys.path.append(a_path)
from ogimet import ogimet


if __name__ == "__main__":
    print "Python OpenMI test implementation"
    print "-" * 40
    print "Station: Krakow"
    print "Local station time: ", \
        str(datetime.datetime.now(pytz.timezone('Europe/Warsaw')))

    component = ogimet.IBaseLinkableComponent()
    component.Initialize()

    ex_item = ogimet.IBaseExchangeItem()
    value_set = ogimet.IBaseValueSet()
    value = value_set.GetValue()

    print "Recent air temperature: {t} [C]".format(t=value)
    component.Finish()