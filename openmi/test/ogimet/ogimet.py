#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Ogimet - test OpenMI linkable component

Return last observed temperature at station Krakow (PL)
"""

__author__      = "Robert Szczepanek"
__email__       = "robert@szczepanek.pl"

import sys
import os
a_path = os.path.abspath('../../..')
sys.path.append(a_path)

from openmi import openmi
import getdata

#define dimension for temperature
temp_dim = openmi.IDimension()
temp_dim.dimension_base['temperature'] = 1

#define temperature unit
class IUnit(openmi.IUnit):
    def ConversionFactorToSI():
        return 1
    def OffSetToSI():
        return -273.15
    def Dimension():
        return temp_dim

temp_unit = IUnit()
temp_unit.SetCaption('temperature')
temp_unit.SetDescription('Temperature in C')

#define value type
class IValueDefinition(openmi.IValueDefinition):
    def ValueType(self):
        return type(float)

temp_value = IValueDefinition()

#define quantity interface
class IQuantity(openmi.IQuantity):
    def Unit():
        return temp_unit

temperature = IQuantity()

#define linkable component
class IBaseLinkableComponent(openmi.IBaseLinkableComponent):
    def Finish(self):
        print 'Finish ...'

    def Initialize(self):
        print 'Initialize ...'

    def Outputs(self):
        return temp_output


temp_link = IBaseLinkableComponent('temperature')

#define echange item
class IBaseExchangeItem(openmi.IBaseExchangeItem, IValueDefinition):
    def Component(self):
        return temp_link

temp_ex_item = IBaseExchangeItem(id=22)

#define values from Ogimet
class IBaseValueSet(object):
    def __init__(self):
        data = getdata.get_station_data(12566, 2)       # Kraków
        self.temp = getdata.temperature(data)

    def GetValue(self):
        return self.temp

temp_value = IBaseValueSet()
#temp_value.SetValue()


class IBaseOutput(IBaseExchangeItem, IBaseValueSet):
    def Values():
        return IBaseValueSet.GetValue()

temp_output = IBaseOutput(22)


if __name__ == "__main__":

    data = getdata.get_station_data(12566, 2)       # Kraków
    temp = getdata.temperature(data)
    #print temp