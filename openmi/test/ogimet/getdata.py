#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Ogimet - OpenMI linkable component"""

__author__      = "Robert Szczepanek"
__email__       = "robert@szczepanek.pl"

import urllib2
import time
import datetime


SERVER = 'http://www.ogimet.com/cgi-bin/getsynop?'
# http://www.ogimet.com/getsynop_help.phtml.en


def getFromUrl(url):
    try:
        page = urllib2.urlopen(url)
    except:
        print '... connection problem ...'
        time.sleep(2)
        return ''
    page = page.read()
    return page


def get_date(offset=0):
    """ Return current day and time. Offset for shift [h] """

    delta = datetime.timedelta(hours = offset)
    current = datetime.datetime.now()
    x = current - delta

    return x.strftime("%Y"), x.strftime("%m"), x.strftime("%d"), x.strftime("%H"), x.strftime("%M")


def get_station_data(station_id, time_horizon=8):
    """ Return last recorded temperature at station_id. Time_horizon shift for start period """

    cYear, cMonth, cDay, cHour, cMinute = get_date()   # current date-time
    sYear, sMonth, sDay, sHour, sMinute = get_date(time_horizon)   # shifted date-time
    req = "begin=" + sYear + sMonth + sDay + sHour + "00&block=" + str(station_id)

    dData = getFromUrl(SERVER + req)
    d = dData.splitlines()

    return d


def temperature(data):
    """ Return temperature [C] """
    
    selected = data[-1]   # get just last observation

    ele = selected.split(' ')
    for ele2 in ele[5:6]:    # at 5th position
        v = ele2[2:]
        if ele2[:2] == '10':
            if v.isdigit():
                return float(v) / 10.
            else:
                return None
        elif ele2[:2] == '11':
            if v.isdigit():
                return - float(v) / 10.
            else:
                return None
    return None
    