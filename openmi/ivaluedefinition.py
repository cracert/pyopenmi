#!/usr/bin/python
# -*- coding: utf-8 -*-
"""IValueDefinition class"""


"""
Copyright (c) 2005-2010, OpenMI Association
"http://www.openmi.org/"
Python version by Robert Szczepanek, 2011-2014
"http://www.openhydrology.org/"

This file is __NOT__YET__ part of OpenMI.Standard2

OpenMI.Standard2 is free software; you can redistribute it and/or modify
it under the terms of the Lesser GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

OpenMI.Standard2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
Lesser GNU General Public License for more details.

You should have received a copy of the Lesser GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from idescribable import IDescribable

class IValueDefinition(IDescribable):
    """
    A ValueDefinition describes a value returned by the getValues method of a
    IExchangeItem. This interface is not meant to be implemented
    directly. Instead, implement either IQuality or IQuantity, or
    a custom derived IValueDefinition interface.
    """

    def ValueType(self):
        """
        The object types of value that will be available in the IBaseValueSet that is
        returned by the Values property and the GetValues function of the IBaseExchangeItem.
        """
        pass

    def MissingDataValue(self):
        """
        The value representing that data is missing
        """
        pass
