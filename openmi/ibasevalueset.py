#!/usr/bin/python
# -*- coding: utf-8 -*-
"""IBaseValueSet class"""


"""
Copyright (c) 2005-2010, OpenMI Association
"http://www.openmi.org/"
Python version by Robert Szczepanek, 2011-2014
"http://www.openhydrology.org/"

This file is __NOT__YET__ part of OpenMI.Standard2

OpenMI.Standard2 is free software; you can redistribute it and/or modify
it under the terms of the Lesser GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

OpenMI.Standard2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
Lesser GNU General Public License for more details.

You should have received a copy of the Lesser GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


class IBaseValueSet(object):
    """
    To enable massive data exchange over a link, an interface structure has been defined that tightly
    matches to the typical implementation of computational cores, often computed per time step. This
    interface (IValueSet) represents an ordered two-dimensional list of values. The first dimension stands
    for the times for which values are available, whereas in the second dimension each value belongs to
    precisely one element in the corresponding element set (that was specified when asking for the
    values). In other words, the i-th value in that dimension of the value set corresponds to the i-th
    element in the element set.
    """

    def GetIdexCount():
        """

        GetIdexCount(int[]) : int
        """

    def GetValue():
        """
        GetValue(int[]) : object
        """

    def SetValue():
        """

        SetValue(int[], object) : void
        """

    def NumbertOfIndices():
        """

        NumbertOfIndices() : int
        """

    def ValueType():
        """

        ValueType() : Type
        """
