#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Copyright (c) 2005-2010, OpenMI Association
"http://www.openmi.org/"
Python version by Robert Szczepanek, 2011-2014
"http://www.openhydrology.org/"

This file is __NOT__YET__ part of OpenMI.Standard2

OpenMI.Standard2 is free software; you can redistribute it and/or modify
it under the terms of the Lesser GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

OpenMI.Standard2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
Lesser GNU General Public License for more details.

You should have received a copy of the Lesser GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


class IDimension(object):
    """ Defines the order of dimension in each dimension_base for a unit. """
    
    dimension_base = {
        'length' : 0,     # m
        'mass' : 0,       # kg
        'time' : 0,       # s
        'electric_current' : 0,    # A
        'temperature' : 0,        # K
        'amount_of_substance' : 0,  # mol
        'luminous_intensity' : 0,  # cd
        'currency' : 0   # E
        }
    
  
    def GetPower(self, base_quantity):
        """             
        Returns the power for the requested dimension.
        
        For a quantity such as flow, which may have the unit m3/s

        Python example: 
        >>> myDimension.GetPower('length')
        3
        >>> myDimension.GetPower('time')
        -1
        >>> myDimension.GetPower('mass')
        0

        C# example:
        GetPower method must work as follows:
        myDimension.GetPower(DimensionBase.AmountOfSubstance) -->returns 0
        myDimension.GetPower(DimensionBase.Currency) --> returns 0
        myDimension.GetPower(DimensionBase.ElectricCurrent) --> returns 0
        myDimension.GetPower(DimensionBase.Length) --> returns 3
        myDimension.GetPower(DimensionBase.LuminousIntensity) --> returns 0
        myDimension.GetPower(DimensionBase.Mass) --> returns 0
        myDimension.GetPower(DimensionBase.Temperature) --> returns 0
        myDimension.GetPower(DimensionBase.Time) --> returns -1
        """

        return self.dimension_base[base_quantity]
        

if __name__ == "__main__":        
    dim_temp = IDimension()
    dim_temp.dimension_base['length'] = 3
    print dim_temp.GetPower('length')
